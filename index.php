<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conseils Soutenances</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-kQtW33rZJAHjgefvhyyzcGF3C5TFyBQBA13V1RKPf4uH+bwyzQxZ6CmMZHmNBEfJ" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
include ('construct.php');

echo '<nav class="navbar navbar-expand-lg navbar-light bg-light shadow mb-3">
        <div class="container-fluid">

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">';
foreach ($categories as &$cat)
{
   
        echo '<li class="nav-item dropdown me-2">
                <a class="navbar-brand dropdown-toggle ms-2 me-2" href="#'.$cat->id.'" id="navbar'.$cat->id.'" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                '.$cat->nom.'
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbar'.$cat->id.'">';
                foreach( $cat->cards as &$card)
                {   
                    echo '<li><a class="dropdown-item " href="#" data-bs-toggle="modal" data-bs-target="#'.$card->id.'_Modal">'.$card->nom.'</a></li>';
                }
                echo '<li><a class="dropdown-item " href="#" data-bs-toggle="modal" data-bs-target="#'.$cat->id.'_Source_Modal">Sources</a></li>';
               echo '</ul>
            </li>';
}
echo '</div>
        </div>
        </div>
        </nav>';

echo '<div id="main" class="d-flex flex-column align-items-center">';

foreach ($categories as &$cat)
{
    $cat->toString();
    $cat->printSourceModal();
}
echo '</div>';

echo '<div id="modals">';
foreach ($categories as &$cat)
{
   foreach( $cat->cards as &$card)
   {
       $card->printModal();
   }
}
echo '</div>';


?>
 <div class="card-footer text-muted text-center mt-2">
            Elie & Nicolas </br>
    <small>  2021-2022 </small> 
  </div>
</body>
<script>
  
  onglets = document.querySelectorAll('.onglet')
  collapses = document.querySelectorAll('.collapse')
    for(onglet of onglets)
    { 
         onglet.onclick = function ()
         {
            for(elem of collapses){
                if(elem.id != this.dataset.bsTarget)
                {
                    elem.classList.remove('show')
                }
            } 
           
         }
    }

</script>
</html>