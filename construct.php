
<?php
require 'categorie.php';
require 'card.php';

$cards = array();
$categories = array();

//$nom,$modalContenu,$soustitre,$question
$nom = 'Introduction';
$modalContenu = '<p>En quelques mots, présentez-vous, puis l\'entreprise et enfin votre démarche de recherche de stage.</p>
                 <p>Vous pourrez évoquer votre environnement, mais cela doit rester succinct.</p>';
$soustitre = 'L\'importance de la première impression';
$question ='Que retrouver dans votre introduction ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Le sujet';
$modalContenu = '<p>Votre sujet doit être, soit la problématique principale de votre stage/alternance, soit votre mission principale.</p>
                <p>Une bonne approche est de présenter votre sujet en miroir d\'un besoin de l\'entreprise</p>
                <p>Il est très important que l\'interet de votre travail soit compris par votre auditoire.</p> ';
$soustitre = 'L\'accroche de votre présentation';
$question ='Quelle doit être la ligne directrice de votre présentation ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'L\'analyse';
$modalContenu = '<p>Après avoir présenté votre sujet, il est nécessaire de décrire la vision que vous en aviez, les étapes par lesquelles vous êtes passer afin d\'atteindre une compréhension complète de vos problématiques, vos enjeux.</p>
                 <p>Dans un second temps, il peut être envisagé d\'expliciter vos pistes de réflexions des éventuels solutions que vous avez entreprie.</p>';
$soustitre = 'Ou votre compréhension du Sujet';
$question ='Comment avez-vous abordé votre sujet ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Un point technique';
$modalContenu = '<p>Le point technique doit être un exemple parmi les solutions que vous avez utilisés. </p>
                <p>Il faudra expliquer à la fois votre choix, sa mise en place, ainsi que son utilisation et éventuellement sa maintenabilité.</p>';
$soustitre = 'Un Challenge particulier';
$question ='Comment choisir son exemple ? À quel niveau de détails doit-on s\'arreter ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Le bilan';
$modalContenu = '<p>Vous pouvez commencer par un rapide résumé.</p>
                 <p>Ensuite argumenté, ce qui selon vous, vous a apporté ce stage. Et ce, d\'un point de vue humain, professionnel et des compétences acquises</p>';
$soustitre = 'Une Conclusion';
$question ='Qu\'avez vous retiré de cette expérience ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Demonstration';
$modalContenu = '<p>La démonstration est un moyen supplémentaire de faire comprendre votre travail.</p>
                 <p>Il faut néanmoins avoir conscience qu\'elle n\'est pas obligatoire, il ne faut pas se reposer uniquement sur elle pour assurer la compréhension de votre travail.</p>
                 <p>Elle permet néanmoins de mettre en avant un élément particulier, donc de valoriser votre réussite.</p>
                 <p>Éviter une présentation des méthodes CRUD.</p>';
$soustitre = 'Un exemple Concret';
$question ='Comment imager et approfondir votre présentation ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$sourceModal = '<p>
                    <embed src="/site-communication/documents/Projet_contenu_soutenance.pdf?zoom=400"  ALIGN=CENTER class="w-100" height="500"  type="application/pdf"/>
                </p>
                <a href="/site-communication/documents/Projet_contenu_soutenance.pdf" download class="btn btn-primary d-block m-auto ">
                    Telecharger
                </a>';


$categories[] = new Categorie('Contenu',$cards,$sourceModal);

//Categorie presentation
$cards = array();

$nom = 'Règles Powerpoint';
$modalContenu = '<p>La première page doit impérativement contenir : 
    <ul>
        <li>Le nom de l\'entreprise</li>
        <li>La ville</li>
        <li>Le département</li>
        <li>En sous titre : Sujet du stage</li>
    </ul></p>
    <p>1 diapo = 1 idée !</p>
    <p>Sur chaque slide , inscrire quelques mots ou une image, mais <strong>jamais</strong> un texte</p>
    <p>La <strong>lisibilité</strong> doit être votre mot d\'ordre! Garder votre présentation <strong>sobre</strong> et <strong>simple</strong> </p>
    <p>Les images doivent êtres pertinentes pour apporter du vivant</p>';
$soustitre = 'Les incoutournables';
$question ='Quels sont les indispensables d\'un powerpoint?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Attitude';
$modalContenu = '<p>Votre attitude dois respirer le <strong>dynamisme!</strong> Cela nécessite donc de s\'<strong>entrainer</strong> afin de bien maitriser votre présentation</p>
                <p>Lire est proibé, encore une fois dans un souci de dynamisme et pour assurer de capter l\'attention de l\'audience</p>
                <p>Chercher le <strong>contact visuel</strong>, pour vous adapter à l\'attitude de vos interlocuteurs</p>
               ';
$soustitre = 'Communication non verbal';
$question ='Comment appuyer votre discours avec votre attitude ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Placement et Gestuel';
$modalContenu = '<p>Placer vous sur le coté de vos visuel</p>
                 <p>Vous pouvez vous deplacez, mais avec modération</p>
                 <p>Lors d\'une description de schema ou d\'un visuel, pointer ce dont vous parlez</p>
                 <p>Quand vous exprimez une idée, utilisé des gestes correspondant: exemple, pour parler d\'inclusion on va écarter les bars pour ensuite les ramener vers vous</p>
                 ';
$soustitre = 'En complément de l\'attitude';
$question ='Comment utilisé votre corps pour appuyer votre discours ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Questions';
$modalContenu = '<p>Anticiper les questions les plus classiques, afin de préparer vos réponses à l\'avance</p>
                 <p>Lors de la présentation vous pouvez ommetre volontairement certain détails afin de dévelloper ceux ci lors de l\'échange</p>
                  ';
$soustitre = 'Interaction direct';
$question ='Comment se préparer aux questions ? Comment les esquiver ou les orienter ';


$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$sourceModal = '<p>
                    <embed src="/site-communication/documents/Projet_Forme_presentation_soutenance.pdf?zoom=400"  ALIGN=CENTER class="w-100" height="500"  type="application/pdf"/>
                </p>
                <a href="/site-communication/documents/Projet_Forme_presentation_soutenance.pdf" download class="btn btn-primary d-block m-auto ">
                    Telecharger
                </a>';

$categories[] = new Categorie('Forme',$cards,$sourceModal);
//CATEGORIE LANGUE

$cards = array();

$nom = 'Généralités';
$modalContenu = '<p>Il faut bien articuler afin d\'être facilement audible</p>
                 <p>Adressez vous directement au jury</p>
                 <p>Vous devez décrire votre suport , surtout lorsque vous présentez un schéma, mais évitez absolument toute lecture</p>
                 <p>Le niveau de langage doit être correct et professionel </p>';
$soustitre = 'Le BA-BA de l\'expression oral';
$question ='Quels doivent être vos réflexes à l\'oral ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Adaptation';
$modalContenu = '<p>Choisissez un vocabulaire selon à qui vous vous adressez. Technique pour un professionel ou généraliste pour un public large</p>
                 <p>Incorporez un peu de redondance afin d\'insister sur les points important de votre présentation, ou bien d\'expliciter différemment un point technique</p>
                 <p>Il est possible de dynamiser votre présentation en créant des interactions, si votre public est réceptif. N\'insistez pas trop si vous sentez que ce n\'est pas le cas</p>
                 ';
$soustitre = 'S\'adresser à une audience particulière';
$question ='Comment adapter sont discours selon l\'audience ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);


$sourceModal = '<p>
                    <button class="btn btn-primary onglet" type="button" data-bs-toggle="collapse" data-bs-target="#collapseDoc1" aria-expanded="false" aria-controls="collapseDoc1" ">
                        Document n°1
                    </button>
                    <button class="btn btn-primary onglet" type="button" data-bs-toggle="collapse" data-bs-target="#collapseDoc2" aria-expanded="false" aria-controls="collapseDoc2" ">
                        Document n°2
                    </button>
                </p>
                    <div class="collapse" id="collapseDoc1" >
                        <div class="card card-body">
                            <p>
                                <embed src="/site-communication/documents/Projet_Langue_soutenance.pdf?zoom=400"  ALIGN=CENTER class="w-100" height="500"  type="application/pdf"/>
                            </p>
                            <a href="/site-communication/documents/Projet_Langue_soutenance.pdf" download class="btn btn-primary d-block m-auto ">
                                Telecharger
                            </a>
                        </div>
                    </div>
                    <div class="collapse" id="collapseDoc2" >
                        <div class="card card-body">
                            <p>
                                <embed src="/site-communication/documents/Projet_Langue_soutenance(2).pdf?zoom=400"  ALIGN=CENTER class="w-100" height="500"  type="application/pdf"/>
                            </p>
                            <a href="/site-communication/documents/Projet_Langue_soutenance(2).pdf" download class="btn btn-primary d-block m-auto ">
                                Telecharger
                            </a>
                        </div>
                    </div>';


$categories[] = new Categorie('Langue',$cards,$sourceModal);

//Categorie evaluation 
$cards = array();

$nom = 'Généralités';
$modalContenu = '<p>Il faut biien lire votre sujet car les modalitées peuevnt variées</p>
                 <p>Si vous avez un doute sur un point, posez des questions à vos référents avant l\'évaluations</p>
                 <p>Si il vous à été fourni une grille d\'évaluations, prenez le temps de l\'analyser</p>
                 ';
$soustitre = 'Quelques directives';
$question ='Comment comprendre votre évaluation ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Préparation';
$modalContenu = '<p>Entrainez vous! Plus vous serez à l\'aise, plus la présentation sera facile pour vous!</p>
                 <p>Si vous connaissez votre lieu de passage, allez sur place repérer les lieux, et voir quels matériels sera à votre disposition.</p>
                 <p>Respecter le temps imparti, pensez à vous chronométrer !</p>';
$soustitre = 'Astuces pour la réussite';
$question ='Comment bien se préparer ?';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);

$nom = 'Vidéo Introductions';
$modalContenu = '<iframe src="https://youtube.com/embed/a2hTNgY6C7E"> </iframe>';
$soustitre = 'Complément d\'informations';
$question ='Une autre source pour des conseils sur les introductions';

$cards[] = new Card($nom,$modalContenu,$soustitre,$question);


$sourceModal = '<p>
                    <embed src="/site-communication/documents/Projet_evaluation_soutenance.pdf?zoom=400"  ALIGN=CENTER class="w-100" height="500"  type="application/pdf"/>
                </p>
                <a href="/site-communication/documents/Projet_evaluation_soutenance.pdf" download class="btn btn-primary d-block m-auto ">
                    Telecharger
                </a>';


$categories[] = new Categorie('Evaluation',$cards,$sourceModal);

