<?php

class Card {
    public $nom ;
    public $modalContenu;
    public $id;
    public $soustitre;
    public $question;

   function __construct($nom,$modalContenu,$soustitre,$question)
   {
       $this->nom = $nom;
       $this->soustitre = $soustitre;
       $this->id =  preg_replace('/[^A-Za-z0-9\-]/', '', $nom);
       $this->modalContenu = $modalContenu;
       $this->question = $question;
   }
   function toString()
   {
        echo    '<div class="card  m-3 border border-secondary card-conseil shadow">
                    <div class="card-header"> <h4>'.$this->nom.'</h4></div>
                    <div class="card-body">
                        <h6 class="card-title mb-3">'.$this->soustitre.'</h6>
                        <a href="#" class="stretched-link" data-bs-toggle="modal" data-bs-target="#'.$this->id.'_Modal" ></a>
                        <p class="card-text mb-3">'.$this->question.'</p>
                    </div>
                </div>';
   }
   function printModal()
   {
        echo ' <div class="modal fade" id="'.$this->id.'_Modal" tabindex="-1" aria-labelledby="'.$this->id.'_ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header ">
                            <h5 class="modal-title " id="'.$this->id.'_ModalLabel">'.$this->nom.'</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
                        </div>
                        <div class="modal-body">
                            <p>'.$this->modalContenu.'</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        </div>
                        </div>
                    </div>
                </div>';
   }

}
