<?php

class Categorie {
    public $nom ;
    public $cards;
    public $id;
   
   function __construct($nom,$cards,$sourceModal)
   {
       $this->nom = $nom;
       $this->id =  preg_replace('/[^A-Za-z0-9\-]/', '', $nom);
       $this->cards = $cards;
       $this->sourceModal = $sourceModal;
   }
   function toString()
   {
        echo '<div class="card border-light categorie m-3 w-75">
                <div class="card-header text-center" id="'.$this->id.'"> <h1>'.$this->nom.'</h1></div>
                <div class="card-body d-flex flex-row justify-content-center flex-wrap ">';
       foreach($this->cards as &$card)
       {
           $card->toString();
       }
       echo '   </div>
                <div class="card-footer border-light bg-light  class="stretched-link" data-bs-toggle="modal" data-bs-target="#'.$this->id.'_Source_Modal"">
                    Sources
                </div>
            </div>';
        
   }
   function printSourceModal()
   {
        echo ' <div class="modal fade" id="'.$this->id.'_Source_Modal" tabindex="-1" aria-labelledby="'.$this->id.'_Source_ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header ">
                            <h5 class="modal-title " id="'.$this->id.'_Source_ModalLabel">Sources</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fermer"></button>
                        </div>
                        <div class="modal-body d-flex flex-column">
                            <p>
                            <p>Télecharger le document ci-dessous : </p> '.
                                $this->sourceModal
                            .'</p>
                        </div>
                        </div>
                    </div>
                </div>';
   }

}
